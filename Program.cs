//Uses NET 5.0

using System;
using System.IO;
using System.Security.Cryptography;

namespace SHA256HashDatabase
{
    class Program
    {
        static void Main(string[] args)
        {            
            if (args.Length != 2)
            {
                printError();
                return;
            }

            Console.WriteLine("\nProcessing... This could take a very long while depending on how many files need to be processed");

            string filePath = args[0];
            string targetFilePath = args[1] + ".shadb";

            using (FileStream fs = File.Create(targetFilePath))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    foreach (string file in Directory.EnumerateFiles(filePath, "*", SearchOption.AllDirectories))
                    {
                        sw.WriteLine(new HashSizeObject(file).ToString());
                    }
                }
                fs.Close();
            }

            Console.WriteLine("Finished!");
        }

        static void printError()
        {
            Console.WriteLine(
                    "\n" +
                    "Required structure of input: \n" +
                    "command [source path to process] [target file] \n" +
                    "\n" +
                    "Example: \n" +
                    "SHA256Database.exe \\ISOFolder\\LinuxDistros \\TargetFolder\\HashDatabase \n" +
                    "\n" +
                    "In this example, the program will hash all files inside the folder called LinuxDistros recursively and output the result to a newly created file called HashDatabase.shadb inside the folder called TargetFolder \n"
                    );
        }
    }

    class HashSizeObject
    {
        public byte[] hashValue;
        public long fileSize;
        public string fileName;
        public string parentDirectory;

        public HashSizeObject(string filePath)
        {
            FileInfo fi = new FileInfo(filePath);

            hashValue = computeSHA256(filePath);
            fileSize = fi.Length;
            fileName = fi.Name;
            parentDirectory = fi.Directory.Name;
        }

        public override string ToString()
        {
            return Convert.ToHexString(hashValue) + "\t?\t" + fileSize + "\t?\t" + fileName + "\t?\t" + parentDirectory;
        }

        public static byte[] computeSHA256(string filePath)
        {

            SHA256 mySHA256 = SHA256Managed.Create();
            byte[] hashValue;

            using (FileStream filestream = new FileStream(filePath, FileMode.Open)
            {
                Position = 0
            })
            {
                hashValue = mySHA256.ComputeHash(filestream);
                filestream.Close();
            }

            return hashValue;
        }
    }
}
